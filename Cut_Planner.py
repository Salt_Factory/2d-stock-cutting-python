# Cut_Planner.py
# Author: Salt_Factory
# https://gitlab.com/Salt_Factory
# simonvandevelde.be


import copy
import random
import sys

BL_THIC = 0


class Shape:
    """
    Class which represents a shape to cut out of a larger board.
    At creation a width, height and a unique index is needed.

    The longest_edge and area is also calculated, which are used respectively
    as primary and secondary key to sort the shapes.

    """
    def __init__(self, width, height, index):
        self.width = width
        self.height = height
        self.longest_edge = max(width, height)
        self.area = width*height
        self.index = index
        self.turned = False

    def __str__(self):
        return "Shape{}".format(self.index)

    def turn(self):
        """
        Turns the shape, so that its width becomes its height and vice versa.
        """
        temp = self.width
        self.width = self.height
        self.height = temp
        self.turned = not self.turned


class Board:
    """
    The class representing the board where the shapes are supposed to be cut
    out of.
    At creating it needs to be supplied the width, height an a unique index.

    The corners list contains all the coordinates where a new shape could be
    placed. The list is initialised with the board's top-left corner.
    """
    def __init__(self, width, height, index):
        self.width = width
        self.height = height
        self.index = index
        self.corners = [Corner(0, 0)]  # Top-left corner.
        self.assigned = {}  # Maps a shape index on coordinates.
        self.area = width*height
        self.frame = 0

    def __str__(self):
        string = "Board{}:\n".format(self.index)
        for index, coor in self.assigned.items():
            string += "Shape{} at {}, {}\n".format(
                    index,
                    coor[0],
                    coor[1])
        return string

    def fit(self, corner, shape):
        """
        Checks whether a shape fits at a specific corner.
        The x/y of the corner + the shape width/height cannot exceed the total
        board width/height.
        Returns True if it fits, False if it does not.
        """
        if corner not in self.corners:
            print("Unexisting corner")
            return False
        if corner.x + shape.width > self.width:
            print("Failed width constraint")
            return False
        if corner.y + shape.height > self.height:
            print("Failed height constraint")
            return False
        return True

    def overlap(self, tl1, br1):
        """
        Checks for 2 Corner-objects whether they overlap with any shapes on the
        board.

        Returns True if there's an overlap present, False if there's none.
        """
        # Iterate over all shapes, check whether they overlap
        for corners in self.assigned.values():
            tl2, br2 = corners
            print(tl2, br2)
            if (br2.y <= tl1.y - BL_THIC or br1.y <= tl2.y - BL_THIC):
                continue
            elif (br2.x <= tl1.x - BL_THIC or br1.x <= tl2.x - BL_THIC):
                continue
            else:
                return True
        return False

    def assign(self, shape_index, tl_corner, br_corner):
        """
        Assigns a shape_index to a cornerpair.

        First it adds the shape to the assigned list, then it removes the
        corner at which the shape was placed.
        After which it calculates the 3 new corners, and adds those to the
        list.
        """
        if tl_corner not in self.corners:
            raise ValueError("Assigning tl corner not in cornerlist")

        # Add the new shape.
        self.assigned[shape_index] = [tl_corner, br_corner]
        print("Assigned shape{} to board{} at".format(shape_index, self.index))

        # Remove the top-left corner.
        self.corners.remove(tl_corner)

        # Create and add the other three corners
        bl_corner = Corner(tl_corner.x, br_corner.y + BL_THIC)
        tr_corner = Corner(br_corner.x + BL_THIC, tl_corner.y)
        br_corner = Corner(br_corner.x + BL_THIC, br_corner.y + BL_THIC)

        self.corners.append(tr_corner)
        self.corners.append(bl_corner)
        self.corners.append(br_corner)
        print("Added {}, {}, {}".format(str(bl_corner), str(tr_corner),
                                        str(br_corner)))

        # draw_frame(self, self.frame, path="Img/")
        self.frame = self.frame + 1


class Corner:
    """
    Class to represent a corner of a shape, consists of an x and a y.
    """
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return "Corner at ({};{})".format(self.x, self.y)

    def calculate_area(self, corner):
        """
        Calculate the area between this corner and another corner.
        """
        return abs(self.x-corner.x) * abs(self.y-corner.y)

    def to_tuple(self):
        return (self.x, self.y)

    def add(self, width, height):
        """
        Calculate a new corner at a specific width and height.
        """
        return Corner(self.x+width, self.y+height)


def place_shape(use_board, shape_list, next_shape_list, sol_list, index):
    print("Placing shape at index{}".format(index))
    if index == len(shape_list):
        print("Filled a board!")
        if next_shape_list != []:
            print(next_shape_list)
        sol_list.append(use_board)
        # sys.exit()
        return True
    shape = shape_list[index]
    board = copy.copy(use_board)
    corners = copy.copy(board.corners)

    for corner in board.corners:
        if not board.fit(corner, shape):
            print("Shape{} doesn't fit on board{}".format(shape.index,
                                                          board.index))
            continue

        # Calculate the corners of the shape we're adding
        tl_corner = corner
        br_corner = corner.add(shape.width, shape.height)

        # Find out whether it overlaps any of the other assigned shapes
        if board.overlap(tl_corner, br_corner):
            print("Overlap on board{}".format(board.index))
            continue

        # If the shape fits, and there's no overlap detected, we can add it.
        board.assign(shape.index, tl_corner, br_corner)

        # Recursiooooooon.
        if place_shape(copy.deepcopy(use_board),
                       copy.deepcopy(shape_list),
                       next_shape_list,
                       sol_list,
                       index + 1):
            return True
        board.corners = corners

    # Turn the shape, try again
    shape.turn()
    if __debug__:
        print("Turned shape")
    for corner in board.corners:
        if not board.fit(corner, shape):
            print("Shape{} doesn't fit on board{}".format(shape.index,
                                                          board.index))
            continue

        # Calculate the corners of the shape we're adding
        tl_corner = corner
        br_corner = corner.add(shape.width, shape.height)

        # Find out whether it overlaps any of the other assigned shapes
        if board.overlap(tl_corner, br_corner):
            print("Overlap on board{}".format(board.index))
            continue

        # If the shape fits, and there's no overlap detected, we can add it.
        board.assign(shape.index, tl_corner, br_corner)

        # Recursiooooooon.
        if place_shape(copy.deepcopy(use_board),
                       copy.deepcopy(shape_list),
                       next_shape_list,
                       sol_list,
                       index + 1):
            return True
        board.corners = corners

    next_shape_list.append(shape)
    if place_shape(copy.deepcopy(use_board),
                   copy.deepcopy(shape_list),
                   next_shape_list,
                   sol_list,
                   index + 1):
        return True
    return


class Cut_Planner:
    def __init__(self, shape_list=[], board_list=[]):
        print("Initialised Cut_Planner")
        self.shape_list = shape_list
        self.board_list = board_list
        self.board_index = 0
        self.shape_index = 0
        self.sol_list = []

    def __enter__(self):
        print("Init!")
        return self

    def __exit__(self, a, b, c):
        del self

    def __str__(self):
        return str(self.shape_list) + str(self.board_list)

    def clear(self):
        self.board_index = 0
        self.shape_index = 0
        self.shape_list.clear()
        self.board_list.clear()
        self.sol_list.clear()

    def add_shape(self, width, height):
        shape = Shape(width, height, self.shape_index)
        self.shape_list.append(shape)
        self.shape_index = self.shape_index + 1
        return self.shape_index - 1

    def add_board(self, width, height):
        board = Board(width, height, self.board_index)
        self.board_list.append(board)
        self.board_index = self.board_index + 1

    def plan_cuts(self):
        self.board_index = 0
        self.sol_list = []
        if self.board_list == [] or self.shape_list == []:
            raise ValueError("Need at least one shape and at least one board.")
        # First sort the list on area, then sort the list on longest edge.
        # Sorting is stable, so when 2 shapes have the same longest_edge,
        # they remain sorted by area as secondary key.
        self.shape_list.sort(key=lambda shape: shape.area, reverse=True)
        self.shape_list.sort(key=lambda shape: shape.longest_edge,
                             reverse=True)

        self.board_list.sort(key=lambda board: board.area, reverse=True)
        next_shape_list = copy.copy(self.shape_list)
        while next_shape_list != []:
            shapes = copy.copy(next_shape_list)
            next_shape_list = []

            # Create a new board if necessary
            if self.board_index >= len(self.board_list):
                # Create a new board
                print("Created new board")
                width = self.board_list[0].width
                height = self.board_list[0].height
                board = Board(width, height, self.board_index)
                self.board_list.append(board)

            place_shape(self.board_list[self.board_index], shapes,
                        next_shape_list, self.sol_list, 0)
            self.board_index = self.board_index + 1
            # input()

        print("All done! Fits on {} boards".format(self.board_index))
        # print(self.sol_list)
        return self.solution_to_dict(self.sol_list)

    def draw_solution(self):
        draw_boards(self.sol_list)

    def solution_to_dict(self, sol):
        """
        Formats the solution to:
            {board_index: [[boardwidth, boardheight], [shape_index, (x1,y1),
                           (x2, y2)], [..], ..}]
        """
        sol_list = {}
        for board in sol:
            positions = [[board.width, board.height]]
            for shape in board.assigned.items():
                shape_index = shape[0]
                corners_list = shape[1]

                position = [shape_index]
                for corner in corners_list:
                    position.append(corner.to_tuple())
                positions.append(position)
            sol_list[board.index] = positions
        return sol_list

def main():
    cp = Cut_Planner()

    cp.add_board(2500, 600)
    cp.add_shape(600, 800)
    cp.add_shape(400, 800)
    cp.add_shape(400, 800)
    cp.add_shape(600, 800)
    cp.add_shape(600, 800)
    cp.add_shape(600, 400)
    cp.add_shape(100, 800)
    cp.add_shape(82, 800)
    cp.add_shape(746, 100)
    cp.add_shape(1342, 100)
    cp.add_shape(2200, 600)
    cp.add_shape(1500, 300)
    cp.add_shape(1500, 400)
    cp.add_shape(600, 800)

    cp.plan_cuts()
    cp.draw_solution()



if __name__ == "__main__":
    main()
