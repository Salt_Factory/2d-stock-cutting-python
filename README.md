# 2D Stock Cutting in Python

This is a small implementation of a 2D stock cutting algorithm in Python.

The algorithm used is something of my own design, and is far from optimal.
It's a greedy algorithm that will try to fit the largest pieces first, 
and creates new boards whenever a large piece can't fit. 

To use it, simply clone the git repo, import **Cut_Planner**, add at least one 
board to cut from and at least one shape to fit on the board(s).

Example:

```
from Cut_Planner import Cut_Planner

def main():
    cp = Cut_Planner.Cut_Planner()
	
    cp.add_board(150, 150)  # Add a 1500 by 1500 board.
	
    cp.add_shape(25, 50)  # Add shapes.
    cp.add_shape(25, 75)
    cp.add_shape(50, 50)
    cp.add_shape(100, 50)
    cp.add_shape(50, 100)
    cp.add_shape(75, 75)
    cp.add_shape(25, 25)
    cp.add_shape(25, 25)
	
    cp.plan_cuts()
    cp.draw_solution()  # Requires OpenCV to draw.
```

This codesnippet results in the following layout:

![alt text](./example.png)

Adding a shape with width 150 and height 60 would result in the following layout:

![alt text](./example2.png)    ![alt text](./example3.png)
## The algorithm

The algorithm starts out by sorting the shapes, on two attributes:

 * The secondary sort on area of the shapes, so that shapes with the biggest area come first.
 * The primary sort on the longest edge of the shapes, so that shapes with the longest edges come first in the list.
 
Because the Python sort holds stability, we can firstly sort on area, followed by longest edge.
This way, if two shapes have the same length for their longest edge, they will automatically be sorted between themselves based on area.

Now it can start assigned the shapes to positions. 
Each board holds a list of **Corners** (which each hold an x and a y value) where shapes can be assigned to. Initially, this list contains only (0, 0), or the top-left corner of the board. 

The algorithms tries to assign the shapes recursively to corners. To be able to be assigned to a corner, a shape needs to fit on the board (not cross its width and/or height), and can't have any overlaps with already assigned shapes.
If a shape is assigned, the corner it's assigned to is removed from the list and three new corners are added: the top-right, bottom-right and bottom-left corner of the newly assigned shape.
If a shape can't be assigned to a corner, it searches for the next corner. If there are no corners where it can be assigned, the shape is turned (so that the width and height are inverted), and each available corner is tried again.

Finally, if there is no corner corner that fits, then the lastly added shape is removed, and the algorithm tries to reassign it to a different corner.
If no single position for a shape can be found, even when altering the others, it's stored on a seperate list and the algorithm proceeds to fit the other shapes.

Once all the shapes have been tried, the algorithm creates a new board and tries to assign all shapes on the seperate list on that board. 
Rinse and repeat this process, and when it's done it will have assigned all shapes on one or more boards.

Below gif demonstrates the concept: a shape is assigned, and it's three corners added (the red circles).

![alt text](./example.gif)

The algorithm used is far from optimal, and the concrete implementation also isn't perfect.
However, it works relatively fast and delivers decent results.

If you think of any improvements or find any bugs, please don't hesitate to make a merge request or an issue report. Thanks!


My motivation for this project is being able to cut wooden planks out of boards more easily and more efficiently. 
In my spare time I like to build furniture with my father, and I hope this project can help you as much as it has helped us.