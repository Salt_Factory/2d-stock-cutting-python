import Cut_Planner


def main():
    cp = Cut_Planner.Cut_Planner()

    cp.add_board(150, 150)
    cp.add_shape(25, 50)
    cp.add_shape(25, 75)
    cp.add_shape(50, 50)
    cp.add_shape(100, 50)
    cp.add_shape(50, 100)
    cp.add_shape(75, 75)
    cp.add_shape(25, 25)
    cp.add_shape(25, 25)

    cp.add_shape(150, 60)

    print(cp.plan_cuts())
    # cp.draw_solution()



if __name__ == "__main__":
    main()
